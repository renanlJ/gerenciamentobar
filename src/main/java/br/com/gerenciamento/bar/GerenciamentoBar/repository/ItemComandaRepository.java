package br.com.gerenciamento.bar.GerenciamentoBar.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gerenciamento.bar.GerenciamentoBar.model.ItemComanda;

@Repository
public interface ItemComandaRepository extends CrudRepository<ItemComanda, Long>{
	
	@Query("select i from ItemComanda i where i.id = ?1")
	ItemComanda buscaPorId(Long id); 
	
	@Query(value = "SELECT COMANDA FROM ITEM_COMANDA WHERE ID = ?1", nativeQuery = true)
	Long buscaComandaId(Long id); 
	
}
