package br.com.gerenciamento.bar.GerenciamentoBar.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.gerenciamento.bar.GerenciamentoBar.model.Cliente;
import br.com.gerenciamento.bar.GerenciamentoBar.model.Conta;
import br.com.gerenciamento.bar.GerenciamentoBar.repository.ClienteRepository;
import br.com.gerenciamento.bar.GerenciamentoBar.repository.ContaRepository;

@Controller
@RequestMapping("/clientes")
public class ClienteController {
	
	@Autowired
	private ClienteRepository repo;
	
	@Autowired
	private ContaRepository contaRepo;	
	
	@PostMapping("/filtrado")
	public ModelAndView visualizarFiltrados(@RequestParam("nome") String nome) {
		
		Iterable<Cliente> clientes = repo.buscarPorNomeIgnoreCase(nome.toUpperCase());

		ModelAndView mv = new ModelAndView("/cliente_visualizar");
		mv.addObject("clientes", clientes);

		return mv;
	}
	
	@GetMapping("/add")
	public ModelAndView add(Cliente cliente) {
		
		ModelAndView mv = new ModelAndView("/cliente_incluir");
		mv.addObject("cliente", cliente);
		
		return mv;
	}
	
	@PostMapping("/save")
	public ModelAndView save(@Valid Cliente cliente, BindingResult result) {
		
		if(result.hasErrors())
			return add(cliente);
		
		repo.save(cliente);
		
		/*
		 * Ao inserir um cliente uma nova conta é criada para o mesmo.
		 * O tratamento abaixo tem o objetivo de evitar a criação de uma 
		 * nova conta para um mesmo cliente ao realizar a edição do cadastro do cliente.
		 */
		if(cliente.getConta() == null) {
			Conta conta = new Conta();
			cliente.setConta(conta);
			contaRepo.save(conta);
			repo.save(cliente);
		}
		
		return findAll();
	}
	
	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		return add(repo.buscarPorId(id));
	}
	
	@GetMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		
		repo.deleteById(id);
		
		return findAll();
	}
	
	@GetMapping("/")
	private ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("/cliente_visualizar");
		mv.addObject("clientes", repo.findAll());
		
		return mv;
	}

}
