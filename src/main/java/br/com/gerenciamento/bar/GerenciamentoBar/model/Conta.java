package br.com.gerenciamento.bar.GerenciamentoBar.model;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "conta")
public class Conta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	private boolean estaDevendo;

	private BigDecimal valorTotal;

	@OneToOne(mappedBy="conta")
	private Cliente cliente;

	@Temporal(TemporalType.DATE)
	private Calendar data;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataHora;

	@Temporal(TemporalType.DATE)
	private Calendar dataAtualizacao;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataHoraAtualizacao;

	public Conta() {
		super();
		
		Calendar instance = Calendar.getInstance();
		this.data = instance;
		this.dataAtualizacao = instance;
		this.dataHora = instance;
		this.dataHoraAtualizacao = instance;
		
		calcularValorTotal();
		this.valorTotal = getValorTotal();
		this.estaDevendo = isEstaDevendo();
	}
	
	public Conta(Cliente cliente) {
		super();
		this.cliente = cliente;
		
		Calendar instance = Calendar.getInstance();
		this.data = instance;
		this.dataAtualizacao = instance;
		this.dataHora = instance;
		this.dataHoraAtualizacao = instance;
		
		calcularValorTotal();
		this.valorTotal = getValorTotal();
		this.estaDevendo = isEstaDevendo();
	}

	public Conta(long id, boolean estaDevendo, Calendar data, Calendar dataHora, Calendar dataAtualizacao,
			Calendar dataHoraAtualizacao) {
		super();
		this.id = id;
		this.estaDevendo = estaDevendo;
		this.data = data;
		this.dataHora = dataHora;
		this.dataAtualizacao = dataAtualizacao;
		this.dataHoraAtualizacao = dataHoraAtualizacao;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public Calendar getDataHora() {
		return dataHora;
	}

	public void setDataHora(Calendar dataHora) {
		this.dataHora = dataHora;
	}

	public Calendar getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Calendar dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Calendar getDataHoraAtualizacao() {
		return dataHoraAtualizacao;
	}

	public void setDataHoraAtualizacao(Calendar dataHoraAtualizacao) {
		this.dataHoraAtualizacao = dataHoraAtualizacao;
	}

	public boolean isEstaDevendo() {
		estaDevendo = getValorTotal().compareTo(BigDecimal.ZERO) != 0;
		return estaDevendo;
	}

	public BigDecimal getValorTotal() {
		return this.valorTotal;
	}
	
	public Cliente getCliente() {
		return this.cliente;
	}
	
	public BigDecimal calcularValorTotal() {
		valorTotal = BigDecimal.ZERO;

		if (cliente != null && cliente.getComandas() != null) {
			for (Comanda comanda : cliente.getComandas()) {
				if (!comanda.isEstaPaga())
					valorTotal = valorTotal.add(comanda.getValorTotal());
			}
		}

		return valorTotal;
	}
}
