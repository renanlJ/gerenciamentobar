package br.com.gerenciamento.bar.GerenciamentoBar.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity @Table(name="item_comanda")
public class ItemComanda {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	private Produto produto;
	
	private int quantidade;
	
	private BigDecimal subTotal;
	
	@ManyToOne
	private Comanda comanda;
	
	public ItemComanda() {
		super();
	}
	public ItemComanda(Produto produto, int quantidade) {
		super();
		this.produto = produto;
		this.quantidade = quantidade;
		
		calcularSubTotal();
		this.subTotal = getSubTotal();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public BigDecimal getSubTotal() {		
		return this.subTotal;
	}
	public BigDecimal calcularSubTotal() {
		subTotal = BigDecimal.ZERO;
				
		subTotal = produto.getValor().multiply(new BigDecimal(quantidade));
		
		return subTotal;
	}
	public Comanda getComanda() {
		return this.comanda;
	}
}