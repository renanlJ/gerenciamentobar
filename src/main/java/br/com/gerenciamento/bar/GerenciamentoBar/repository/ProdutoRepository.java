package br.com.gerenciamento.bar.GerenciamentoBar.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gerenciamento.bar.GerenciamentoBar.model.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Long>{
	@Query("select p from Produto p where p.id = ?1")
	Produto buscarPorId(Long id);
	
	@Query("select p from Produto p where UPPER(p.nome) LIKE CONCAT (?1, '%')")
	List<Produto> buscarPorNomeIgnoreCase(String nome);
}
