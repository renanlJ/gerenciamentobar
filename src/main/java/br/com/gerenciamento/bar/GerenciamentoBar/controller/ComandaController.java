package br.com.gerenciamento.bar.GerenciamentoBar.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import br.com.gerenciamento.bar.GerenciamentoBar.model.Cliente;
import br.com.gerenciamento.bar.GerenciamentoBar.model.Comanda;
import br.com.gerenciamento.bar.GerenciamentoBar.model.ItemComanda;
import br.com.gerenciamento.bar.GerenciamentoBar.model.Produto;
import br.com.gerenciamento.bar.GerenciamentoBar.repository.ClienteRepository;
import br.com.gerenciamento.bar.GerenciamentoBar.repository.ComandaRepository;
import br.com.gerenciamento.bar.GerenciamentoBar.repository.ItemComandaRepository;
import br.com.gerenciamento.bar.GerenciamentoBar.repository.ProdutoRepository;

@Controller
@RequestMapping("/comanda")
public class ComandaController {

	@Autowired
	private ComandaRepository comandaRepo;

	@Autowired
	private ClienteRepository cliRepo;

	@Autowired
	private ProdutoRepository produtoRepo;
	
	@Autowired
	private ItemComandaRepository itemRepo;
	
	@GetMapping("/visualizar/")
	public ModelAndView visualizarComandas() {
		
		Iterable<Comanda> comandas = comandaRepo.findAll();

		ModelAndView mv = new ModelAndView("/comanda_visualizar_todas");
		mv.addObject("comandas", comandas);

		return mv;
	}

	@GetMapping("/cliente")
	public ModelAndView add() {

		Iterable<Cliente> clientes = cliRepo.findAll();

		ModelAndView mv = new ModelAndView("/comanda_cliente");
		mv.addObject("clientes", clientes);

		return mv;
	}
	
	@GetMapping("/pagar/{id}")
	public RedirectView pagar(@PathVariable String id) {

		Comanda comanda = comandaRepo.buscarPorId(Long.parseLong(id));
		comanda.setEstaPaga(true);
		
		comanda.getCliente().getConta().calcularValorTotal();
		comanda.getCliente().getConta().isEstaDevendo();
		
		comandaRepo.save(comanda);		

		RedirectView rv = new RedirectView();
		rv.setUrl("/comanda/visualizar/" + comanda.getId());

		return rv;
	}
	
	@PostMapping("/filtrado")
	public ModelAndView visualizarFiltrados(@RequestParam("nome") String nome) {
		
		Iterable<Comanda> comandas = comandaRepo.buscarPorNomeIgnoreCase(nome.toUpperCase());

		ModelAndView mv = new ModelAndView("/comanda_visualizar_todas");
		mv.addObject("comandas", comandas);

		return mv;
	}
	
	@GetMapping("/cancelar/{id}")
	public RedirectView cancelar(@PathVariable String id) {

		Comanda comanda = comandaRepo.buscarPorId(Long.parseLong(id));
		comanda.setEstaPaga(true);
		
		comanda.getCliente().getConta().calcularValorTotal();
		comanda.getCliente().getConta().isEstaDevendo();
		
		comandaRepo.delete(comanda);
		
		RedirectView rv = new RedirectView("/comanda/cliente/");

		return rv;
	}
	
	@GetMapping("/delete/item/{id}")
	public RedirectView edit(@PathVariable("id") Long id) {
		
		ItemComanda itemComanda = itemRepo.buscaPorId(id);
		Long idComanda = itemRepo.buscaComandaId(id);
		itemRepo.delete(itemComanda);
		
		Comanda comanda = comandaRepo.buscarPorId(idComanda);
		comanda.calcularValorTotal();
		comanda.getCliente().getConta().calcularValorTotal();
		comanda.getCliente().getConta().isEstaDevendo();
		
		comandaRepo.save(comanda);
		
		RedirectView rv = new RedirectView("/comanda/visualizar/" + idComanda);
		
		return rv;
	}

	@PostMapping("/criar")
	public RedirectView novaComanda(Model model, HttpServletRequest request) {

		Cliente cliente = cliRepo.buscarPorId(Long.parseLong(request.getParameter("cliente")));
		Comanda novaComanda = new Comanda(cliente);
		comandaRepo.save(novaComanda);
		
		RedirectView rv = new RedirectView();
		rv.setUrl("/comanda/visualizar/" + novaComanda.getId());

		return rv;
	}

	@GetMapping("/visualizar/{id}")
	public ModelAndView visualizar(@PathVariable String id) {
		
		Comanda comanda = comandaRepo.buscarPorId(Long.parseLong(id));

		ModelAndView mv = new ModelAndView("/comanda_visualizar");
		mv.addObject("comanda", comanda);

		return mv;
	}

	@GetMapping("/adiciona/{id}")
	public ModelAndView adicionarItem(@PathVariable("id") String id) {
		
		Iterable<Produto> produtos = produtoRepo.findAll();
		
		ModelAndView mv = new ModelAndView("/item_incluir");
		mv.addObject("produtos", produtos);
		mv.addObject("comanda", id);

		return mv;
	}
	
	@GetMapping("/{id}/edit/{item}")
	public ModelAndView editarItem(@PathVariable("id") String id, 
										@PathVariable("item") String itemId) {
		
		ItemComanda itemComanda = itemRepo.buscaPorId(Long.parseLong(itemId));
		
		Iterable<Produto> produtos = produtoRepo.findAll();
		
		ModelAndView mv = new ModelAndView("/item_editar");
		mv.addObject("produtos", produtos);
		mv.addObject("comanda", id);
		mv.addObject("item",itemComanda);

		return mv;
	}

	@PostMapping("/item/incluir")
	public RedirectView itemIncluir(@RequestParam("comanda") Long comandaId, 
									@RequestParam("produto") Long produtoId,
									@RequestParam("quantidade") Integer quantidade) {
		
		Comanda comanda = comandaRepo.buscarPorId(comandaId);
		Produto produto = produtoRepo.buscarPorId(produtoId);
		
		ItemComanda itemComanda = new ItemComanda(produto, quantidade);
		
		itemRepo.save(itemComanda);
		
		if(comanda.getItens() != null) {
			comanda.getItens().add(itemComanda);
		}
		
		comanda.calcularValorTotal();
		comanda.getCliente().getConta().calcularValorTotal();
		comanda.getCliente().getConta().isEstaDevendo();
		
		comandaRepo.save(comanda);
		
		RedirectView rv = new RedirectView("/comanda/visualizar/" + comanda.getId());
		
		return rv;
	}
	
	@PostMapping("/item/editar/{id}")
	public RedirectView itemEditar(@RequestParam("comanda") Long comandaId, 
									@RequestParam("produto") Long produtoId,
									@RequestParam("quantidade") Integer quantidade,
									@PathVariable String id) {
		
		Comanda comanda = comandaRepo.buscarPorId(comandaId);
		Produto produto = produtoRepo.buscarPorId(produtoId);
		
		ItemComanda itemComanda = itemRepo.buscaPorId(Long.parseLong(id));
		itemComanda.setProduto(produto);
		itemComanda.setQuantidade(quantidade);
		
		itemRepo.save(itemComanda);
		
		comanda.calcularValorTotal();
		comanda.getCliente().getConta().calcularValorTotal();
		comanda.getCliente().getConta().isEstaDevendo();
		
		comandaRepo.save(comanda);
		
		RedirectView rv = new RedirectView("/comanda/visualizar/" + comanda.getId());
		
		return rv;
	}

}
