package br.com.gerenciamento.bar.GerenciamentoBar.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.gerenciamento.bar.GerenciamentoBar.model.Produto;
import br.com.gerenciamento.bar.GerenciamentoBar.repository.ProdutoRepository;

@Controller
@RequestMapping("/produtos")
public class ProdutoController {
	
	@Autowired
	private ProdutoRepository repo;
	
	@PostMapping("/filtrado")
	public ModelAndView visualizarFiltrados(@RequestParam("nome") String nome) {
		
		Iterable<Produto> produtos = repo.buscarPorNomeIgnoreCase(nome.toUpperCase());

		ModelAndView mv = new ModelAndView("/produto_visualizar");
		mv.addObject("produtos", produtos);

		return mv;
	}
	
	@GetMapping("/add")
	public ModelAndView add(Produto produto) {
		
		ModelAndView mv = new ModelAndView("/produto_incluir");
		mv.addObject("produto", produto);
		
		return mv;
	}
	
	@PostMapping("/save")
	public ModelAndView save(@Valid Produto produto, BindingResult result) {
		
		if(result.hasErrors()) {
			return add(produto);
		}
		
		repo.save(produto);
		
		return findAll();
	}
	
	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		return add(repo.buscarPorId(id));
	}
	
	@GetMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		
		repo.deleteById(id);
		
		return findAll();
	}
	
	@GetMapping("/")
	private ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("/produto_visualizar");
		mv.addObject("produtos", repo.findAll());
		
		return mv;
	}

}
