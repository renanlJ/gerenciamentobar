package br.com.gerenciamento.bar.GerenciamentoBar.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gerenciamento.bar.GerenciamentoBar.model.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long>{
	
	@Query("select c from Cliente c where c.id = ?1")
	Cliente buscarPorId(Long id);
	
	@Query("select c from Cliente c where UPPER(c.nome) LIKE CONCAT (?1, '%')")
	List<Cliente> buscarPorNomeIgnoreCase(String nome);

}
