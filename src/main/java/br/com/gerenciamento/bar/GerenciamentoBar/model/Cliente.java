package br.com.gerenciamento.bar.GerenciamentoBar.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity @Table(name="cliente")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String nome;
	
	private String numeroCelular;
	
	@OneToOne
	private Conta conta;
	
	@OneToMany(mappedBy="cliente")
	private List<Comanda> comandas;
	
	public Cliente() {
		super();
	}

	public Cliente(Long id, String nome, String numeroCelular, Conta conta, List<Comanda> comandas) {
		super();
		this.id = id;
		this.nome = nome;
		this.numeroCelular = numeroCelular;
		this.conta = conta;
		this.comandas = comandas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public List<Comanda> getComandas() {
		return comandas;
	}

	public void setComandas(List<Comanda> comandas) {
		this.comandas = comandas;
	}	
}
