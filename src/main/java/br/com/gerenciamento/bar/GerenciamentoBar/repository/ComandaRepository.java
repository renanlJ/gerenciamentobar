package br.com.gerenciamento.bar.GerenciamentoBar.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gerenciamento.bar.GerenciamentoBar.model.Comanda;

@Repository
public interface ComandaRepository extends CrudRepository<Comanda, Long>{
	
	@Query("select c from Comanda c where c.id = ?1")
	Comanda buscarPorId(Long id);
	
	@Query("select c from Comanda c where UPPER(c.cliente.nome) LIKE CONCAT (?1, '%')")
	List<Comanda> buscarPorNomeIgnoreCase(String nome);
	
}
