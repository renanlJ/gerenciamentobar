package br.com.gerenciamento.bar.GerenciamentoBar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.gerenciamento.bar.GerenciamentoBar.model.Cliente;
import br.com.gerenciamento.bar.GerenciamentoBar.model.Conta;
import br.com.gerenciamento.bar.GerenciamentoBar.repository.ContaRepository;
import br.com.gerenciamento.bar.GerenciamentoBar.wsclient.EnviaSMS;

@Controller
@RequestMapping("/contas")
public class ContaController {

	@Autowired
	private ContaRepository contaRepo;

	@GetMapping("/listar")
	public ModelAndView listarContas() {
		Iterable<Conta> contas = contaRepo.findAll();

		ModelAndView mv = new ModelAndView("/contas");
		mv.addObject("contas", contas);

		return mv;
	}

	@PostMapping("/filtrado")
	public ModelAndView visualizarFiltrados(@RequestParam("nome") String nome) {

		Iterable<Conta> contas = contaRepo.buscarPorNomeIgnoreCase(nome.toUpperCase());

		ModelAndView mv = new ModelAndView("/contas");
		mv.addObject("contas", contas);

		return mv;
	}

	@GetMapping("/cobrar/{id}")
	public ModelAndView cobrar(@PathVariable String id) {

		long idConta = Long.parseLong(id);

		Cliente cliente = contaRepo.buscarClientePorContaId(idConta);

		if (cliente.getNumeroCelular() != null && !cliente.getNumeroCelular().equals("0")) {
			Conta conta = contaRepo.buscarContaPorId(idConta);

			String mensagem = "Olá, " + cliente.getNome() 
					+ "! O valor sua conta no " + "Bar do Coito está em: "
					+ conta.getValorTotal();

			EnviaSMS.sendSms(mensagem, cliente.getNumeroCelular());
		}
		
		return listarContas();
	}

}
