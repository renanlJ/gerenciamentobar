package br.com.gerenciamento.bar.GerenciamentoBar.wsclient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
 
public class EnviaSMS {
	
	public static String sendSms(String mensagem, String numeroCliente) {
		try {
			// Construct data
			String apiKey = "apikey=" + "rSoMQ5yPs2I-zNikeZF9Q9jv1ruQpGlmnlFZ12vnz5";
			String message = "&message=" + mensagem;
			String sender = "&sender=" + "Bar_do_Coito";
			String numbers = "&numbers=5511" + numeroCliente;
			
			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://api.txtlocal.com/send/?").openConnection();
			String data = apiKey + numbers + message + sender;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			
			return stringBuffer.toString();
		} catch (Exception e) {
			System.out.println("Error SMS " + e);
			return "Error " + e;
		}
	}
}
