package br.com.gerenciamento.bar.GerenciamentoBar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GerenciamentoBarApplication {

	public static void main(String[] args) {
		SpringApplication.run(GerenciamentoBarApplication.class, args);
	}
}
