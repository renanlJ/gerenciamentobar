package br.com.gerenciamento.bar.GerenciamentoBar.model;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity @Table(name = "comanda")
public class Comanda {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@OneToMany(orphanRemoval = true, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "comanda")
	private List<ItemComanda> itens;

	private boolean estaPaga;

	private BigDecimal valorTotal;

	@OneToOne
	private Usuario atendente;

	@ManyToOne
	private Cliente cliente;

	@Temporal(TemporalType.DATE)
	private Calendar data;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataHora;

	@Temporal(TemporalType.DATE)
	private Calendar dataAtualizacao;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataHoraAtualizacao;

	public Comanda() {
		super();
	}
	
	public Comanda(Cliente cliente) {
		super();
		this.cliente = cliente;
		
		Calendar instance = Calendar.getInstance();
		this.data = instance;
		this.dataAtualizacao = instance;
		this.dataHora = instance;
		this.dataHoraAtualizacao = instance;
		this.estaPaga = false;
		
		calcularValorTotal();
		this.valorTotal = getValorTotal();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ItemComanda> getItens() {
		return itens;
	}

	public void setItens(List<ItemComanda> itens) {
		this.itens = itens;
	}

	public BigDecimal getValorTotal() {
		return this.valorTotal;
	}
	
	public BigDecimal calcularValorTotal() {
		valorTotal = BigDecimal.ZERO;
		
		if (itens != null) {
			for (ItemComanda itemComanda : itens) {
				valorTotal = valorTotal.add(itemComanda.getSubTotal());
			}
		}
		return valorTotal;
	}

	public Usuario getAtendente() {
		return atendente;
	}

	public void setAtendente(Usuario atendente) {
		this.atendente = atendente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public Calendar getDataHora() {
		return dataHora;
	}

	public void setDataHora(Calendar dataHora) {
		this.dataHora = dataHora;
	}

	public Calendar getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Calendar dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Calendar getDataHoraAtualizacao() {
		return dataHoraAtualizacao;
	}

	public void setDataHoraAtualizacao(Calendar dataHoraAtualizacao) {
		this.dataHoraAtualizacao = dataHoraAtualizacao;
	}

	public boolean isEstaPaga() {
		return estaPaga;
	}

	public void setEstaPaga(boolean estaPaga) {
		this.estaPaga = estaPaga;
	}
}
