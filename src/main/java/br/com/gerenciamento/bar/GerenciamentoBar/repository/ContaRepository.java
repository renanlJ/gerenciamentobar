package br.com.gerenciamento.bar.GerenciamentoBar.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gerenciamento.bar.GerenciamentoBar.model.Cliente;
import br.com.gerenciamento.bar.GerenciamentoBar.model.Conta;

@Repository
public interface ContaRepository extends CrudRepository<Conta, Long>{
	
	@Query("select c from Conta c where c.cliente.id = ?1")
	Conta buscarContaPorClienteId(Long id);
	
	@Query("select c from Cliente c where c.conta.id = ?1")
	Cliente buscarClientePorContaId(Long id);
	
	@Query("select c from Conta c where c.id = ?1")
	Conta buscarContaPorId(Long id);
	
	@Query("select c from Conta c where UPPER(c.cliente.nome) LIKE CONCAT (?1, '%')")
	List<Conta> buscarPorNomeIgnoreCase(String nome);

}
